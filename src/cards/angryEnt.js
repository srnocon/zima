const card = {
  type: 'angryEnt',
  name: 'AngryEnt',
  tag: 'enemy',
  attack: 1,
  speed: 2,
  range: 1,
  actionTaken: false,
};

export default card;
