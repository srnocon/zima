const card = {
  type: 'ghost',
  name: 'Ghost',
  tag: 'enemy',
  attack: 1,
  speed: 2,
  range: 3,
  actionTaken: false,
};

export default card;
