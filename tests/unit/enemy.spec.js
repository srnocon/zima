import { expect } from 'chai';
import createArea from '../../src/helpers/createArea';
import createCard from '../../src/helpers/createCard';
import { enemyAction } from '../../src/helpers/enemyAI';

describe('enemy', () => {
  let area;

  beforeEach(() => {
    area = createArea();
  });

  it('given it starts in the north, it moves towards campfire', () => {
    const enemy = createCard('angryEnt');
    area[4][8].cards.push(enemy);

    enemyAction(area);

    expect(area[4][8].cards).to.not.contain(enemy);
    expect(area[4][7].cards).to.contain(enemy);
  });
});
