import areaStore from './areaStore';
import createCard from './createCard';
import cardStore from './cardStore';

export function spawnEnemy(area) {
  area[4][8].cards.push(createCard('ghost'));
  area[4][8].cards.push(createCard('angryEnt'));
}

export function enemyAction(area) {
  area.forEach(column => {
    column.forEach(tile => {
      tile.cards
        .filter(card => card.tag == 'enemy')
        .forEach(enemyCard => {
          act(enemyCard, tile);
        });
    });
  });
}

function act(enemyCard, tile) {
  if (enemyCard.actionTaken === false) {
    checkEnemyAttack(enemyCard, tile);
    checkEnemyMove(enemyCard, tile);
    resetEnemyAction(enemyCard);
  }
}

function checkEnemyAttack(enemyCard, tile) {
  if (enemyCard.actionTaken === false) {
    const enemyPosition = findEnemyPosition(tile);
    if (enemyPosition === 'east') {
      searchTargetToThe('west', tile, enemyCard);
    } else if (enemyPosition === 'west') {
      searchTargetToThe('east', tile, enemyCard);
    } else if (enemyPosition === 'south') {
      searchTargetToThe('north', tile, enemyCard);
    } else if (enemyPosition === 'north') {
      searchTargetToThe('south', tile, enemyCard);
    }
  }
}

function checkEnemyMove(enemyCard, tile) {
  console.log(enemyCard.actionTaken);
  if (enemyCard.actionTaken === false) {
    enemyCard.actionTaken = true;
    const enemyPosition = findEnemyPosition(tile);
    if (enemyPosition === 'east') {
      moveToThe('west', tile, enemyCard);
    } else if (enemyPosition === 'west') {
      moveToThe('east', tile, enemyCard);
    } else if (enemyPosition === 'south') {
      moveToThe('north', tile, enemyCard);
    } else if (enemyPosition === 'north') {
      moveToThe('south', tile, enemyCard);
    }
  }
}

function findEnemyPosition(tile) {
  if (tile.x > 4) return 'east';
  else if (tile.x < 4) return 'west';
  else if (tile.y < 4) return 'south';
  else if (tile.y > 4) return 'north';
}

function searchTargetToThe(direction, tile, enemyCard) {
  const range = enemyCard.range;
  const area = areaStore.state.area;
  for (var i = 1; i <= range; i++) {
    if (direction === 'west' && area[tile.x - i][tile.y]?.cards[0]?.type === 'campfire') {
      enemyAttack(enemyCard);
      break;
    } else if (direction === 'east' && area[tile.x + i][tile.y]?.cards[0]?.type === 'campfire') {
      enemyAttack(enemyCard);
      break;
    } else if (direction === 'north' && area[tile.x][tile.y + i]?.cards[0]?.type === 'campfire') {
      enemyAttack(enemyCard);
      break;
    } else if (direction === 'south' && area[tile.x][tile.y - i]?.cards[0]?.type === 'campfire') {
      enemyAttack(enemyCard);
      break;
    }
  }
}

function moveToThe(direction, tile, enemyCard) {
  console.log(tile.cards.indexOf(enemyCard));
  tile.cards.splice(tile.cards.indexOf(enemyCard), 1);
  if (direction === 'west') {
    areaStore.state.area[tile.x - 1][tile.y].cards.push(enemyCard);
  } else if (direction === 'east') {
    areaStore.state.area[tile.x + 1][tile.y].cards.push(enemyCard);
  } else if (direction === 'north') {
    areaStore.state.area[tile.x][tile.y + 1].cards.push(enemyCard);
  } else if (direction === 'south') {
    areaStore.state.area[tile.x][tile.y - 1].cards.push(enemyCard);
  }
}

function enemyAttack(enemyCard) {
  cardStore.findCard('campfire').damage(enemyCard.attack);
  enemyCard.actionTaken = true;
}

function resetEnemyAction(enemyCard) {
  enemyCard.actionTaken = false;
}
