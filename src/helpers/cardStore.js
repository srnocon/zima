import createCard from './createCard';
import areaStore from './areaStore';
import { spawnEnemy } from './enemyAI';
//import { enemyAction } from './enemyAI';
import { save } from './persistence';
import { registerStoreForSavingAndRestart } from './persistence';

const cardStore = {
  getStartingState() {
    return {
      deck: [],
      hand: [],
      handSize: 7,
      sideBar: [{ cards: [createCard('clock')] }, { cards: [] }, { cards: [] }],
    };
  },
  restart() {
    this.addCardToDeck('water');
    this.addCardToDeck('food');
    this.addCardToDeck('food');
    this.addCardToDeck('water');
    this.addCardToDeck('mining');
    this.addCardToDeck('mining');
    this.addCardToDeck('torch');
    this.addCardToDeck('food');
    this.addCardToDeck('food');
    this.addCardToDeck('food');
    this.addCardToDeck('food');
    this.addCardToDeck('food');
    spawnEnemy(areaStore.state.area);
    // enemyAction(areaStore.state.area);
    // enemyAction(areaStore.state.area);
    // enemyAction(areaStore.state.area);
    // enemyAction(areaStore.state.area);
    this.drawCard();
    this.drawCard();
    this.drawCard();
    this.drawCard();
    this.drawCard();
    this.drawCard();
    this.drawCard();
  },
  drawCard() {
    if (this.state.deck.length > 0) {
      const drawnCard = this.state.deck.shift();
      this.state.hand.push(drawnCard);
    }
  },
  playCard(card, targetX, targetY) {
    if (card.cost !== undefined) {
      const clock = this.findCard('clock');
      if (clock !== undefined) clock.advanceTime(card.cost);
    }
    if (card.play !== undefined) card.play(areaStore.state.area[targetX][targetY]);
    if (this.state.hand.length < this.state.handSize) this.drawCard();
    save();
  },
  findCard(type) {
    const foundInSideBar = this.state.sideBar
      .flatMap(item => item.cards)
      .find(card => card.type === type);
    if (foundInSideBar !== undefined) return foundInSideBar;
    const foundInHand = this.state.hand.find(card => card.type === type);
    if (foundInHand !== undefined) return foundInHand;
    const foundInArea = areaStore.findCard(type);
    if (foundInArea !== undefined) return foundInArea;
    return undefined;
  },
  addCardToDeck(type) {
    let cards = createCard(type);
    this.state.deck.push(cards);
  },
};

registerStoreForSavingAndRestart('card', cardStore);

export default cardStore;
