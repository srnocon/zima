const ALL_CARDS = [];

export default function getAllCards() {
  if (ALL_CARDS.length == 0) {
    const loader = require.context('../cards', true, /\.js$/);
    loader.keys().forEach(file => ALL_CARDS.push(loader(file).default));
  }
  return ALL_CARDS;
}
