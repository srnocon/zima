import createArea from './createArea';
import { registerStoreForSavingAndRestart } from './persistence';

const areaStore = {
  getStartingState() {
    return {
      area: createArea(),
    };
  },
  findCard(type) {
    for (var y = 0; y < this.state.area.length; y++) {
      var row = this.state.area[y];
      for (var x = 0; x < row.length; x++) {
        if (row[x] === undefined) continue;
        const foundCard = row[x].cards.find(card => card.type === type);
        if (foundCard !== undefined) return foundCard;
      }
    }
    return undefined;
  },
};

registerStoreForSavingAndRestart('area', areaStore);

export default areaStore;
