import cardStore from '../helpers/cardStore';
import gameStore from '../helpers/gameStore';
import areaStore from '../helpers/areaStore';
import { enemyAction } from '../helpers/enemyAI';
//import enemyStore from '../helpers/enemyStore';

const card = {
  type: 'clock',
  name: 'Clock',
  minutes: 17 * 60 + 45,
  survivedMinutes: 0,
  getHours() {
    return Math.floor(this.minutes / 60);
  },
  advanceTime(minutes) {
    const hoursBeforeTimePasses = this.getHours();
    this.minutes += minutes;
    this.survivedMinutes += minutes;
    const fullHoursPassed = this.getHours() - hoursBeforeTimePasses;
    cardStore.findCard('campfire').damage(fullHoursPassed);
    if (this.minutes >= 24 * 60) gameStore.triggerVictory();
    enemyAction(areaStore.state.area);
  },
};

export default card;
