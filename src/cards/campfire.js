import gameStore from '../helpers/gameStore';

const card = {
  type: 'campfire',
  name: 'Campfire',
  value: 100,
  maxValue: 100,
  barColor: 'rgb(255,127,39)',
  locked: true,
  lightLevel: 2,
  damage(points) {
    this.value -= points;
    if (this.value <= 0) {
      this.value = 0;
      gameStore.triggerDefeat();
    }
  },
};

export default card;
