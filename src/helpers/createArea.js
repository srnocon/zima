import createCard from './createCard';

const rows = 9;
const columns = 9;
const centerRow = Math.floor(rows / 2);
const centerColumn = Math.floor(columns / 2);
const treeCount = 20;

export default function createArea() {
  const area = [];
  for (var x = 0; x < rows; x++) {
    area.push([]);
    for (var y = 0; y < columns; y++) {
      area[x].push({ cards: [], x: x, y: y });
    }
  }
  area[centerRow][centerColumn].cards.push(createCard('campfire'));
  fillAreaWithTrees(area);
  return area;
}

function fillAreaWithTrees(area) {
  let treesLeft = treeCount;
  for (var z = 0; z < rows * columns; z++) {
    const tileX = Math.floor(Math.random() * rows);
    const tileY = Math.floor(Math.random() * columns);
    if (tileX !== centerRow && tileY !== centerColumn && area[tileX][tileY].cards.length === 0) {
      area[tileX][tileY].cards.push(createCard('tree'));
      treesLeft--;
    }
    if (treesLeft === 0) break;
  }
}
