import Vue from 'vue';
import App from './App.vue';
// import { isThereASavedGame, load } from './helpers/persistence';

Vue.config.productionTip = false;

// if (isThereASavedGame()) load();

new Vue({
  render: h => h(App),
}).$mount('#app');
