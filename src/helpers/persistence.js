import getAllCards from './getAllCards';

const stores = [];

export function registerStoreForSavingAndRestart(name, store) {
  store.name = name;
  stores.push(store);
  if (store.getStartingState === undefined)
    console.error(`Store ${store} doesn't have getStartingState.`);
  else store.state = store.getStartingState();
  if (store.restart !== undefined) store.restart();
}

export function save() {
  stores.forEach(store => {
    localStorage.setItem(store.name, JSON.stringify(store.state));
  });
  localStorage.setItem('isThereASavedGame', 'true');
}

export function isThereASavedGame() {
  return localStorage.getItem('isThereASavedGame') === 'true';
}

export function load() {
  stores.forEach(store => {
    let loadedState = JSON.parse(localStorage.getItem(store.name));
    iterate(loadedState, loadCardFunctionsThatWereNotSerialized);
    Object.assign(store.state, loadedState);
  });
}

function iterate(object, functionToExecute) {
  functionToExecute(object);
  for (const property in object) {
    if (Array.isArray(object[property]))
      object[property].forEach(element => iterate(element, functionToExecute));
    else if (object[property] instanceof Object) iterate(object[property], functionToExecute);
  }
}

// This is hacky, as it manually loads the functions for cards in the stores.
// A better solution would be more strongly typed deserializer.
function loadCardFunctionsThatWereNotSerialized(potentialCard) {
  if (potentialCard['type'] === undefined) return;
  const correspondingCardTemplate = getAllCards().find(card => card.type === potentialCard['type']);
  for (const property in correspondingCardTemplate) {
    if (typeof correspondingCardTemplate[property] == 'function') {
      potentialCard[property] = correspondingCardTemplate[property];
    }
  }
}

export function restart() {
  stores.forEach(store => {
    deepMerge(store.state, store.getStartingState());
    if (store.restart !== undefined) store.restart();
  });
  save();
  location.reload();
}

const deepMerge = (target, source) => {
  for (const key in source) {
    if (isObject(source[key])) Object.assign(source[key], deepMerge(target[key], source[key]));
  }
  Object.assign(target || {}, source);
  return target;
};

/**
 * Simple object check.
 * @param item
 * @returns {boolean}
 */
export function isObject(item) {
  return item && typeof item === 'object' && !Array.isArray(item);
}
