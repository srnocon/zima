import cardStore from './cardStore';

let currentId = 0;

function generateId() {
  currentId++;
  return currentId;
}
function clone(input) {
  return { ...input };
}

export default function drawDeckCard() {
  let result = clone(cardStore.state.deck[Math.floor(Math.random() * cardStore.state.deck.length)]);
  result.cardId = generateId();
  return result;
}
