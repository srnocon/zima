import { registerStoreForSavingAndRestart } from './persistence';

const gameStore = {
  getStartingState() {
    return {
      isOver: false,
      isRoundWon: false,
    };
  },
  triggerDefeat() {
    this.state.isOver = true;
  },
  triggerVictory() {
    this.state.isRoundWon = true;
  },
};

registerStoreForSavingAndRestart('game', gameStore);

export default gameStore;
